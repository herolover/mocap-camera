#pragma once

#include <list>
#include <memory>

#include <opencv2/opencv.hpp>

#include "../handlers/frame_handler.h"

class FrameReader
{
public:
  virtual ~FrameReader();

  virtual const std::string & name() const = 0;
  virtual cv::Size frame_size() = 0;
  virtual int fps() = 0;

  virtual void start_read(std::list<FrameHandler *> &&frame_handlers) = 0;
  virtual void stop_read() = 0;
};
