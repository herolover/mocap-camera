#include "file_reader.h"

void FileReader::reload()
{
  if (_info.is_open())
    _info.close();
  if (_video.isOpened())
    _video.release();

  _info.open(_filename + ".info");
  _video.open(_filename + ".avi");
}

FileReader::FileReader(const std::string &filename)
  : _filename(filename)
{
  reload();
  _is_read = false;
}

FileReader::~FileReader()
{
  stop_read();
}

const std::string & FileReader::name() const
{
  return _filename;
}

cv::Size FileReader::frame_size()
{
  return cv::Size((int)_video.get(CV_CAP_PROP_FRAME_WIDTH), (int)_video.get(CV_CAP_PROP_FRAME_HEIGHT));
}

int FileReader::fps()
{
  return (int)_video.get(CV_CAP_PROP_FPS);
}

void FileReader::start_read(std::list<FrameHandler *> &&frame_handlers)
{
  stop_read();

  bool expected = false;
  if (_is_read.compare_exchange_strong(expected, true))
  {
    _thread = std::thread([this, frame_handlers = std::move(frame_handlers)]()
    {
      while (_is_read)
      {
        cv::Mat color_frame;
        if (_video.read(color_frame))
        {
          cv::Mat frame;
          cv::cvtColor(color_frame, frame, cv::COLOR_BGR2GRAY);

          long timestamp;
          int capture_time;
          _info >> timestamp >> capture_time;

          for (auto &frame_handler : frame_handlers)
            frame = frame_handler->on_frame(frame, timestamp);
        }
        else
        {
          reload();
          _is_read = false;
        }
      }

      for (auto &frame_handler : frame_handlers)
        delete frame_handler;
    });
  }
}

void FileReader::stop_read()
{
  _is_read = false;
  if (_thread.joinable())
    _thread.join();
}
