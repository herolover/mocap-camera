#pragma once

#include <atomic>
#include <thread>

#include "frame_reader.h"

extern "C"
{
  #include "../camera.h"
}

class DeviceReader: public FrameReader
{
private:
  std::string _device_name;
  std::thread _thread;
  std::atomic_bool _is_read;
  Camera *_camera;

public:
  DeviceReader(const std::string &device_name);
  ~DeviceReader();

  void set_param(CameraParam param, int value);
  int get_param(CameraParam param);

  const std::string & name() const;
  cv::Size frame_size();
  int fps();

  void start_read(std::list<FrameHandler *> &&frame_handlers);
  void stop_read();
};
