#pragma once

#include <fstream>
#include <atomic>
#include <thread>

#include "frame_reader.h"

class FileReader: public FrameReader
{
private:
  std::string _filename;
  std::thread _thread;
  std::ifstream _info;
  cv::VideoCapture _video;
  std::atomic_bool _is_read;

  void reload();
public:
  FileReader(const std::string &filename);
  ~FileReader();

  const std::string & name() const;
  cv::Size frame_size();
  int fps();

  void start_read(std::list<FrameHandler *> &&frame_handlers);
  void stop_read();
};
