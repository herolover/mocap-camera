#include "device_reader.h"

DeviceReader::DeviceReader(const std::string &device_name)
  : _device_name(device_name)
{
  _is_read = false;
  _camera = open_camera(_device_name.c_str());
}

DeviceReader::~DeviceReader()
{
  stop_read();
  close_camera(_camera);
}

void DeviceReader::set_param(CameraParam param, int value)
{
  camera_set_param(_camera, param, value);
}

int DeviceReader::get_param(CameraParam param)
{
  return camera_get_param(_camera, param);
}

const std::string & DeviceReader::name() const
{
  return _device_name;
}

cv::Size DeviceReader::frame_size()
{
  return cv::Size(HARDCODED_IMAGE_WIDTH, HARDCODED_IMAGE_HEIGHT);
}

int DeviceReader::fps()
{
  return get_param(cpFRAME_RATE);
}

void convert_YUV422_to_grayscale(const char *yuv, int yuv_size, char *grayscale)
{
  for (int i = 0; i < yuv_size / 2; i += 2)
  {
    grayscale[i + 0] = yuv[i * 2 + 0];
    grayscale[i + 1] = yuv[i * 2 + 2];
  }
}

void DeviceReader::start_read(std::list<FrameHandler *> &&frame_handlers)
{
  stop_read();

  bool expected = false;
  if (_is_read.compare_exchange_strong(expected, true))
  {
    _thread = std::thread([this, frame_handlers = std::move(frame_handlers)]()
    {
      cv::Size frame_size(HARDCODED_IMAGE_WIDTH, HARDCODED_IMAGE_HEIGHT);
      char grayscale_data[frame_size.area()] = {0};

      camera_start_capture(_camera);
      while (_is_read)
      {
        CameraBuffer *buffer = camera_get_next_filled_frame(_camera);
        if (buffer != nullptr)
        {
          const char *yuv422_data = (const char *)camera_buffer_data(buffer);
          convert_YUV422_to_grayscale(yuv422_data, camera_buffer_length(buffer), grayscale_data);
          cv::Mat frame(frame_size, CV_8UC1, grayscale_data);

          timeval tv = camera_buffer_timestamp(buffer);
          long timestamp = tv.tv_sec * 1000000 + tv.tv_usec;

          for (auto &frame_handler : frame_handlers)
            frame = frame_handler->on_frame(frame, timestamp);

          camera_buffer_release(buffer);
        }
      }
      camera_stop_capture(_camera);

      for (auto &frame_handler : frame_handlers)
        delete frame_handler;
    });
  }
}

void DeviceReader::stop_read()
{
  _is_read = false;
  if (_thread.joinable())
    _thread.join();
}
