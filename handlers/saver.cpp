#include "saver.h"

Saver::Saver(const std::string &record_name, const std::string &camera_id, const cv::Size &frame_size, int fps)
  : _info(record_name + "_" + camera_id + ".info", std::ios_base::trunc)
  , _video(record_name + "_" + camera_id + ".avi", CV_FOURCC('F', 'M', 'P', '4'), fps, frame_size, false)
{
}

cv::Mat Saver::on_frame(const cv::Mat &frame, long timestamp)
{
  if (_prev_timestamp == 0)
    _prev_timestamp = timestamp;

  _info << timestamp << " " << timestamp - _prev_timestamp << "\n";
  _prev_timestamp = timestamp;

  _video.write(frame);

  return frame;
}
