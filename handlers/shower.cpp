#include "shower.h"

Shower::Shower(const std::string &window_name, int on_frame_delay)
  : _window_name(window_name)
  , _on_frame_delay(on_frame_delay)
{
  cv::namedWindow(_window_name);
}

Shower::~Shower()
{
  cv::destroyWindow(_window_name);
}

cv::Mat Shower::on_frame(const cv::Mat &frame, long timestamp)
{
  cv::imshow(_window_name, frame);
  cv::waitKey(_on_frame_delay);

  return frame;
}
