#include "background_subtractor.h"

BackgroundSubtractor::BackgroundSubtractor(const std::string &filename)
{
  cv::VideoCapture capture(filename + ".avi");
  cv::BackgroundSubtractorMOG2 subtractor;

  cv::Mat frame;
  while (capture.read(frame))
  {
    cv::Mat foreground;
    subtractor(frame, foreground);
  }

  cv::Mat color_background;
  subtractor.getBackgroundImage(color_background);

  cv::cvtColor(color_background, _background_image, cv::COLOR_BGR2GRAY);
}

cv::Mat BackgroundSubtractor::on_frame(const cv::Mat &frame, long timestamp)
{
  cv::Mat foreground;
  cv::absdiff(frame, _background_image, foreground);

  return foreground;
}
