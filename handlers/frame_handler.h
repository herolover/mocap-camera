#pragma once

#include <opencv2/opencv.hpp>

class FrameHandler
{
public:
  virtual ~FrameHandler();

  virtual cv::Mat on_frame(const cv::Mat &frame, long timestamp) = 0;
};
