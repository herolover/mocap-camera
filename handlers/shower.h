#pragma once

#include "frame_handler.h"

class Shower: public FrameHandler
{
private:
  std::string _window_name;
  int _on_frame_delay;
public:
  Shower(const std::string &window_name, int on_frame_delay=1);
  ~Shower();

  cv::Mat on_frame(const cv::Mat &frame, long timestamp);
};
