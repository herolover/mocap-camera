#pragma once

#include "frame_handler.h"

#include <opencv2/opencv.hpp>

class BackgroundSubtractor: public FrameHandler
{
private:
  cv::Mat _background_image;
public:
  BackgroundSubtractor(const std::string &filename);

  cv::Mat on_frame(const cv::Mat &frame, long timestamp);
};
