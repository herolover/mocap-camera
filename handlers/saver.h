#pragma once

#include <fstream>

#include <opencv2/opencv.hpp>

#include "frame_handler.h"

class Saver: public FrameHandler
{
private:
  std::ofstream _info;
  cv::VideoWriter _video;

  long _prev_timestamp = 0;
public:
  Saver(const std::string &record_name, const std::string &camera_id, const cv::Size &frame_size, int fps);

  cv::Mat on_frame(const cv::Mat &frame, long timestamp);
};
