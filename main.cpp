#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <atomic>
#include <string>
#include <vector>
#include <memory>

#include <opencv2/opencv.hpp>

#include "readers/device_reader.h"
#include "readers/file_reader.h"
#include "handlers/shower.h"
#include "handlers/saver.h"
#include "handlers/background_subtractor.h"

int main()
{
  std::vector<std::unique_ptr<FrameReader>> frame_readers;

  while (true)
  {
    std::cout << "> ";

    std::string command;
    std::cin >> command;

    if (command == "quit")
      break;
    else if (command == "open")
    {
      std::string device_name;
      std::cin >> device_name;

      auto camera = std::make_unique<DeviceReader>(device_name);
      camera->set_param(cpFRAME_RATE, 60);
      camera->set_param(cpAUTO_EXPOSURE, 0);
      camera->set_param(cpAUTO_GAIN, 0);
      camera->set_param(cpAUTO_WHITE_BALANCE, 0);

      frame_readers.push_back(std::move(camera));
    }
    else if (command == "load")
    {
      std::string filename;
      std::cin >> filename;

      frame_readers.push_back(std::make_unique<FileReader>(filename));
    }
    else if (command == "close")
    {
      for (auto &reader : frame_readers)
        reader->stop_read();
      frame_readers.clear();
    }
    else if (command == "start")
    {
      std::cin >> command;

      if (command == "rec")
      {
        std::string record_name;
        std::cin >> record_name;

        for (auto &reader : frame_readers)
        {
          std::string camera_id = reader->name().substr(reader->name().rfind('/')+1);

          std::list<FrameHandler *> frame_handlers;
          frame_handlers.push_back(new Saver(record_name, camera_id, reader->frame_size(), reader->fps()));
          frame_handlers.push_back(new Shower(reader->name()));
          reader->start_read(std::move(frame_handlers));
        }
      }
      else if (command == "play")
      {
        int on_frame_delay;
        std::cin >> on_frame_delay;

        for (auto &reader : frame_readers)
        {
          std::list<FrameHandler *> frame_handlers;
          frame_handlers.push_back(new Shower(reader->name(), on_frame_delay));
          reader->start_read(std::move(frame_handlers));
        }
      }
      else if (command == "sb")
      {
        std::string background_record_name;
        std::cin >> background_record_name;

        for (auto &reader : frame_readers)
        {
          std::string camera_id = reader->name().substr(reader->name().rfind('/')+1);

          std::string filename = background_record_name + "_" + camera_id;

          std::list<FrameHandler *> frame_handlers;
          frame_handlers.push_back(new BackgroundSubtractor(filename));
          frame_handlers.push_back(new Shower(reader->name()));
          reader->start_read(std::move(frame_handlers));
        }
      }
    }
    else if (command == "stop")
    {
      for (auto &reader : frame_readers)
        reader->stop_read();
    }
    else
    {
      std::cerr << "Undefined command" << std::endl;
    }
  }


  return 0;
}
