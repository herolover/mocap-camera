#include "camera.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <error.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <limits.h>

struct Camera
{
  int fd;
  int buffers_count;
  ParamInfo params[cpCOUNT];
  CameraBuffer* buffers;
};

struct CameraBuffer
{
  int fd;
  struct v4l2_buffer buf;
  void* data;
};

static
void set_image_format(int fd, int width, int height,
                      int pixel_format, int field_order)
{
  struct v4l2_format format;
  memset(&format, 0, sizeof(format));

  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  format.fmt.pix.width = width;
  format.fmt.pix.height = height;
  format.fmt.pix.pixelformat = pixel_format;
  format.fmt.pix.field = field_order;

  if (ioctl(fd, VIDIOC_S_FMT, &format) == -1)
    error(1, errno, "Cannot set image format");
}

static
void init_buffer(int fd, int index, CameraBuffer* buf)
{
  memset(&buf->buf, 0, sizeof(buf->buf));

  buf->fd = fd;
  buf->buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf->buf.memory = V4L2_MEMORY_MMAP;
  buf->buf.index = index;

  if (ioctl(fd, VIDIOC_QUERYBUF, &buf->buf) == -1)
    error(1, errno, "Cannot allocate buffer");

  buf->data = mmap(0, buf->buf.length, PROT_READ | PROT_WRITE,
                   MAP_SHARED, fd, buf->buf.m.offset);
  if (buf->data == MAP_FAILED)
    error(1, errno, "Error while mapping buffer");
}

static
int init_buffers(int fd, int count, CameraBuffer** buffers_out)
{
  struct v4l2_requestbuffers req;
  memset(&req, 0, sizeof(req));

  req.count = count;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;

  if (ioctl(fd, VIDIOC_REQBUFS, &req) == -1)
    error(1, errno, "Error while requesting buffers");

  if (req.count < 5)
    error(1, errno, "Not enough buffers");

  *buffers_out = calloc(req.count, sizeof(CameraBuffer));
  assert(*buffers_out);

  for (size_t i = 0; i < req.count; ++i)
    init_buffer(fd, i, &(*buffers_out)[i]);

  return req.count;
}

static
void enqueue_buffer(int fd, struct v4l2_buffer* buf)
{
  if (ioctl(fd, VIDIOC_QBUF, buf) == -1)
    {
      if (errno == EINVAL)
        error(1, 0, "VIDIOC_QBUF failed because of a programming error");
      else
        error(0, errno, "Error while enqueuing buffer");
    }
}

static
struct {int id; const char* name;}
camera_controls[cpCOUNT] = {
  {V4L2_CID_EXPOSURE_AUTO, "AUTO_EXPOSURE"}, // cpAUTO_EXPOSURE,
  {V4L2_CID_AUTO_WHITE_BALANCE, "AUTO_WHITE_BALANCE"}, // cpAUTO_WHITE_BALANCE,
  {V4L2_CID_AUTOGAIN, "AUTO_GAIN"}, // cpAUTO_GAIN,
  {V4L2_CID_BRIGHTNESS, "BRIGHTNESS"}, // cpBRIGHTNESS,
  {V4L2_CID_CONTRAST, "CONTRAST"}, // cpCONTRAST,
  {V4L2_CID_EXPOSURE, "EXPOSURE"}, // cpEXPOSURE,
  {-1, "FRAME_RATE"} // cpFRAME_RATE,
};

const char* camera_param_name(CameraParam p)
{
  assert(p >= 0 && p < cpCOUNT);
  return camera_controls[p].name;
}

// This function will work only on certain types of controls.
static
void read_control_info(int fd, int control_id, ParamInfo* out)
{
  struct v4l2_queryctrl queryctrl;
  memset(&queryctrl, 0, sizeof(queryctrl));

  queryctrl.id = control_id;

  if (ioctl(fd, VIDIOC_QUERYCTRL, &queryctrl) == -1)
    {
      if (errno != EINVAL)
        error(1, errno, "Error while reading control info");
      return;
    }

  /*
  if (queryctrl.name != 0 && strcmp(queryctrl.name, "") != 0)
    strncpy(out->name, queryctrl.name, CAMERA_CONTROL_NAME_LENGTH);
  */
  if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
    return;

  out->supported = 1;
  out->min = queryctrl.minimum;
  out->max = queryctrl.maximum;
}

static
int get_frame_rate(int fd)
{
  struct v4l2_streamparm streamparm;
  memset(&streamparm, 0, sizeof(streamparm));

  streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl(fd, VIDIOC_G_PARM, &streamparm) == -1)
    return -1;

  return streamparm.parm.capture.timeperframe.denominator;
}

static
int set_frame_rate(int fd, int fps)
{
  struct v4l2_streamparm streamparm;
  memset(&streamparm, 0, sizeof(streamparm));

  streamparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  streamparm.parm.capture.timeperframe.numerator = 1;
  streamparm.parm.capture.timeperframe.denominator = fps;

  return ioctl(fd, VIDIOC_S_PARM, &streamparm);
}

static
void reset_cropping(int fd)
{
  struct v4l2_cropcap cropcap;
  memset (&cropcap, 0, sizeof (cropcap));

  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (ioctl(fd, VIDIOC_CROPCAP, &cropcap) == -1
      && errno != EINVAL
      && errno != ENOTTY)
    error(1, errno, "VIDIOC_CROPCAP");

  struct v4l2_crop crop;
  memset (&crop, 0, sizeof (crop));

  crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  crop.c = cropcap.defrect;

  /* Ignore if cropping is not supported (EINVAL). */
  if (ioctl(fd, VIDIOC_S_CROP, &crop) == -1
      && errno != EINVAL
      && errno != ENOTTY)
    error(1, errno, "VIDIOC_S_CROP");
}

Camera* open_camera(const char* path)
{
  assert(path);

  int fd = open(path, O_RDWR);
  if (fd == -1)
    error(1, errno, "Cannot open device %s", path);

  reset_cropping(fd);

  CameraBuffer* buffers = 0;
  set_image_format(fd,
                   HARDCODED_IMAGE_WIDTH,
                   HARDCODED_IMAGE_HEIGHT,
                   V4L2_PIX_FMT_YUYV,
                   V4L2_FIELD_NONE);
  int buffers_allocated = init_buffers(fd, 10, &buffers);

  Camera* ret = calloc(1, sizeof(Camera));
  assert(ret);

  ret->fd = fd;
  ret->buffers_count = buffers_allocated;
  ret->buffers = buffers;

  for (int i = 0; i < cpCOUNT; ++i)
    {
      strncpy(ret->params[i].name, camera_controls[i].name, CAMERA_CONTROL_NAME_LENGTH);
      if (i != cpFRAME_RATE)
        read_control_info(fd, camera_controls[i].id, &ret->params[i]);
      else
        {
          ret->params[cpFRAME_RATE].supported = 1;
          ret->params[cpFRAME_RATE].min = 0;
          ret->params[cpFRAME_RATE].max = INT_MAX;
        }
    }

  return ret;
}

err_t close_camera(Camera* c)
{
  assert(c);

  camera_stop_capture(c);
  for (int i = 0; i < c->buffers_count; ++i)
    {
      if (munmap(c->buffers[i].data, c->buffers[i].buf.length) == -1)
        error(1, errno, "Error while unmapping buffers");
    }

  while (close(c->fd) != 0)
    {
      if (errno != EINTR)
        error(1, errno, "Error while closing device");
    }

  free(c);

  return 0;
}


err_t camera_start_capture(Camera* c)
{
  assert(c);

  for (int i = 0; i < c->buffers_count; ++i)
    enqueue_buffer(c->fd, &c->buffers[i].buf);

  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  return ioctl(c->fd, VIDIOC_STREAMON, &type);
}

err_t camera_stop_capture(Camera* c)
{
  assert(c);

  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  return ioctl(c->fd, VIDIOC_STREAMOFF, &type);
}

CameraBuffer* camera_get_next_filled_frame(Camera* c)
{
  assert(c);

  struct v4l2_buffer buf;
  memset(&buf, 0, sizeof(buf));

  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;

  if (ioctl(c->fd, VIDIOC_DQBUF, &buf) == -1)
    {
      if (errno == EINVAL)
        error(1, 0, "VIDIOC_DQBUF failed because of a programming error");
      else
        error(0, errno, "Error while dequeuing buffer");

      /*
        TODO
        http://linuxtv.org/downloads/v4l-dvb-apis/vidioc-qbuf.html
        Nevertheless it may dequeue a buffer. The spec does not
        describe how I can determine whether it has done so and
        returned buffer index is correct. In this case the buffer is
        lost so far.
       */

      return 0;
    }

  if (buf.flags & V4L2_BUF_FLAG_ERROR)
    {
      enqueue_buffer(c->fd, &buf);
      return 0;
    }

  c->buffers[buf.index].buf = buf;
  return &c->buffers[buf.index];
}

struct timeval camera_buffer_timestamp(CameraBuffer *b)
{
  assert(b);
  return b->buf.timestamp;
}

void* camera_buffer_data(CameraBuffer* b)
{
  assert(b);
  return b->data;
}

int camera_buffer_length(CameraBuffer* b)
{
  assert(b);
  return b->buf.length;
}


void camera_buffer_release(CameraBuffer* b)
{
  assert(b);

  enqueue_buffer(b->fd, &b->buf);
}

ParamInfo* camera_get_param_info(Camera* c, CameraParam p)
{
  assert(c);
  assert(p >= 0 && p < cpCOUNT);

  return &c->params[p];
}



int camera_get_param(Camera* c, CameraParam p)
{
  assert(c);
  assert(p >= 0 && p < cpCOUNT);

  if (! c->params[p].supported)
    return -1;

  if (p == cpFRAME_RATE)
    {
      int ret = get_frame_rate(c->fd);
      if (ret == -1)
        return -2;
      return ret;
    }

  struct v4l2_control control;
  memset(&control, 0, sizeof(control));

  control.id = camera_controls[p].id;

  if (ioctl(c->fd, VIDIOC_G_CTRL, &control) == -1)
    return -2;

  return control.value;
}


err_t camera_set_param(Camera* c, CameraParam p, int val)
{
  assert(c);
  assert(p >= 0 && p < cpCOUNT);

  if (! c->params[p].supported)
    return -1;

  if (p == cpFRAME_RATE)
    return set_frame_rate(c->fd, val);

  struct v4l2_control control;
  memset(&control, 0, sizeof(control));

  control.id = camera_controls[p].id;
  control.value = val;

  if (ioctl(c->fd, VIDIOC_S_CTRL, &control) == -1)
    return -2;

  return 0;
}

int camera_param_by_name(Camera* c, const char* name)
{
  assert(c);
  assert(name);

  for (int i = 0; i < cpCOUNT; ++i)
    if (strcmp(c->params[i].name, name) == 0)
      return i;

  return -1;
}

void param_info_print(FILE* f, ParamInfo* p)
{
  fprintf(f,
          "%-32s: %c, %d - %d",
          p->name,
          (p->supported ? '+' : '-'),
          p->min,
          p->max);
}
