#ifndef CAMERA_H
#define CAMERA_H

#include <stdio.h>
#include <sys/time.h>

typedef int err_t;

enum {
  CAMERA_CONTROL_NAME_LENGTH = 32, //As defined in v4l2_queryctrl
  HARDCODED_IMAGE_WIDTH = 640,
  HARDCODED_IMAGE_HEIGHT = 480
};

typedef struct Camera Camera;
typedef struct CameraBuffer CameraBuffer;

typedef
enum CameraParam {
  cpAUTO_EXPOSURE,
  cpAUTO_WHITE_BALANCE,
  cpAUTO_GAIN,
  cpBRIGHTNESS,
  cpCONTRAST,
  cpEXPOSURE,
  cpFRAME_RATE,
  cpCOUNT
} CameraParam;

const char* camera_param_name(CameraParam p);

typedef
struct ParamInfo
{
  char name[CAMERA_CONTROL_NAME_LENGTH];
  int supported;

  int min;
  int max;
} ParamInfo;

Camera* open_camera(const char* path);
err_t close_camera(Camera* c);

// -1 - unsupported, -2 - error
int camera_get_param(Camera* c, CameraParam p);
ParamInfo* camera_get_param_info(Camera* c, CameraParam p);
err_t camera_set_param(Camera* c, CameraParam p, int val);
int camera_param_by_name(Camera* c, const char* name);

err_t camera_start_capture(Camera* c);
err_t camera_stop_capture(Camera* c);

CameraBuffer* camera_get_next_filled_frame(Camera* c);
struct timeval camera_buffer_timestamp(CameraBuffer* b);
void* camera_buffer_data(CameraBuffer* b);
int camera_buffer_length(CameraBuffer* b);
void camera_buffer_release(CameraBuffer* b);

void param_info_print(FILE* f, ParamInfo* p);



#endif
