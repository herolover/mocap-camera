# Camera CLI #

### Notes ###
Work only with grayscale frames.

### Available commands ###
* `quit`
* `open` - open a device. Example: `open /dev/video0`.
* `load` - load a record. Example: `load example_video0`.
* `close` - close the all opened devices/records.
* `start play <on_frame_delay>` - play opened devices/records with `<on_frame_delay>` ms delay. If `<of_frame_delay>` is `0` key-press is needed. For device playing it should be `1`, for record playing - it's up to you.
* `start rec <record_name>` - record opened devices/records. On the output for the each device/record will be created a pair of files: `<record_name>_<device/record_name>.info` and `<record_name>_<device/record_name>.avi`. `.info` file contains timestamps and frame capture time. `.avi` file contains a video stream. Number of the frames in both files are the same.
* `start sb <background_record_name>` - compute a background model from `<background_record_name>` and play opened devices/records with background subtraction.
* `stop` - stop all the above commands.