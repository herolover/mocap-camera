import qbs 1.0

Project {
    StaticLibrary {
        name: "mocap-camera"

        Depends {
            name: "cpp"
        }

        files: [
            "camera.h",
            "camera.c"
        ]

        cpp.cFlags: [
            "-std=c99"
        ]

        cpp.linkerFlags: [
        ]
    }

    CppApplication {
        name: "mocap-camera-cli"

        Depends {
            name: "mocap-camera"
        }

        files: [
            "main.cpp",
            "readers/*",
            "handlers/*"
        ]

        cpp.cxxFlags: [
            "-std=c++1y",
            "-Wno-unused-parameter"
        ]

        cpp.linkerFlags: [
            "-lopencv_core",
            "-lopencv_imgproc",
            "-lopencv_highgui",
            "-lopencv_video"
        ]
    }
}
